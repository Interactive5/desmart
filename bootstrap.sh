#!/usr/bin/env bash
rpm -Uvh https://mirror.webtatic.com/yum/el6/latest.rpm
yum -y update
yum -y install epel-release
yum -y upgrade
yum -y install httpd mc htop nano php54w php54w-mcrypt php54w-bcmath php54w-xml

if [ ! -e /etc/httpd/conf.d/desmart.conf ]
    then
        cp /vagrant/conf/desmart.conf /etc/httpd/conf.d/desmart.conf
        chmod 0644 /etc/httpd/conf.d/desmart.conf
fi

if ! [ -e /usr/local/bin/composer ]
    then
        curl -sS https://getcomposer.org/installer | php
        mv composer.phar /usr/local/bin/composer
        chmod a+x /usr/local/bin/composer
fi

if [ ! -d /home/vagrant/public_html ]
    then
        cd /home/vagrant
        mkdir public_html
        chown vagrant:vagrant public_html
fi

if [ -d /home/vagrant/public_html/desmart ]
    then
        cd /home/vagrant/public_html
        chmod 0755 ..

        cd /home/vagrant/public_html/desmart

        /usr/local/bin/composer update
        /usr/local/bin/composer install
fi

service httpd start
service iptables stop

echo -n "All OK"
echo -n "Visit: http://desmart.dev.5ki.pl/"