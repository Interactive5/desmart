[*] 1. Pod adresem "/" ma byc pokazywana lista wszystkich newsów.
[*] 2. Wyświetlić datę na liście newsów. - datę, dzisiejszą? :)
[*] 3. Podpiąć link do podglądu jednego newsa.
[*] 4. Wydzielić zawartość <head/> tak aby można ją było załączać w wielu szablonach.

= Zasady
1. Wolno używać manuala
2. Wolno zmieniać kod (dotyczy to frameworka jak i aplikacji)
  2.1 Nie wolno modyfikować danych (zmienna `$news` w `App\Model\NewsRepo`)
  2.2 Zmiany w frameworku (namespace `DeSmart`) powinny zachować logikę działania
