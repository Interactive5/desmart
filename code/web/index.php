<?php
require_once __DIR__ . '/../vendor/autoload.php';

date_default_timezone_set('Europe/Warsaw');

use DeSmart\View;
use App\Model\NewsRepo as Repo;

$view = new View(__DIR__ . '/../tpl');

$router = new \DeSmart\Router;
$router->get('/tag/{tag}', function ($tag) use ($view) {
    $repo = new Repo();

    $view->render('main.php', array(
        'news' => $repo->getByTag($tag),
        'head' => $view->render('head.php'),
        'back' => true
    ));
});

$router->get('/id/{id}', function ($id) use ($view) {
    $repo = new Repo();

    $view->render('main.php', array(
        'news' => $repo->getById($id),
        'head' => $view->render('head.php'),
        'back' => true
    ));
});

$router->get('/', function () use ($view) {
    $repo = new Repo();

    $view->render('main.php', array(
        'news' => $repo->getAll(),
        'head' => $view->render('head.php'),
        'back' => false
    ));
});
$router->run();
