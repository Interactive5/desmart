<html>
<head>
    <?php echo $head; ?>
</head>

<body>
<div class="container">
    <?php foreach ($news as $item): ?>
        <div class="hero-unit">
            <h2><a href="/id/<?php echo $item->getId(); ?>"><?php echo $item->getTitle(); ?></a></h2>

            <p><?php echo $item->getText(); ?><p>

            <p><?php echo $item->getDate(); ?></p>
        </div>
    <?php endforeach; ?>

    <?php if (true === $back): ?>
        <div class="hero-unit">
            <a href="/">Powrót do listy newsów</a>
        </div>
    <?php endif; ?>
</div>
</body>
</html>
