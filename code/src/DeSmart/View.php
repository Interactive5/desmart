<?php

namespace DeSmart;

class View {

  private $tplDir;

  private $globalVars = array();

  public function __construct($tplDir) {
    $this->tplDir = rtrim($tplDir, '/');
  }

  public function render($template, array $data = null) {
    $path = $this->tplDir.'/'.ltrim($template, '/');
      
    if(null !== $data) {
      $merged = array_merge($this->globalVars, $data);
      extract($merged);
    }

    include $path;
  }

  /**
   * @param string $name
   * @param mixed $value
   * @return View
   */
  public function setGlobal($name, $value) {
    $this->globalVars[$name] = $value;

    return $this;
  }

}
