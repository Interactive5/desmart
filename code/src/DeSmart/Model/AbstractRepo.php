<?php

namespace DeSmart\Model;

abstract class AbstractRepo {

  /**
   * Opakowuje dane z $data w obiekt.
   *
   * Nazwa klasy z ktorej powstaje obiekt jest generowana na podstawie
   * nazwy klasy Repo z ktorej wyciagane sa dane
   *
   * @param array $data
   * @return AbstractEntity
   */
  protected function hydrate(array $data) {
    $model_name = str_replace('Repo', '', get_class($this));

    return new $model_name($data);
  }

}
