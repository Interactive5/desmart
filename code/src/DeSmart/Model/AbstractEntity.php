<?php

namespace DeSmart\Model;

abstract class AbstractEntity {

  public final function __construct(array $item) {
    
    foreach($item as $key => $value) {
      $this->{$key} = $value;
    }
  }

  public function __get($name) {
    throw new \LogicException('Nope');
  }

  public function __set($name, $value) {
    throw new \LogicException('Nope');
  }

  /**
   * Automagiczny setter/getter
   *
   * Przykladowe wywolania metod:
   * - setPole($wartosc) -> $this->pole = $wartosc; return $this
   * - getPole() -> return $this->pole
   *
   * @param string $name
   * @param array $arguments
   * @return mixed|AbstractEntity
   */
  public function __call($name, $arguments) {
    $match = array();

    if(false == preg_match('/^(set|get)(\w+)/', $name, $match)) {
      throw new \BadMethodCallException('Unknown method call');
    }

    $field = lcfirst($match[2]);

    // gdy pole nie zostalo zadeklarowane wyrzuc wyjatek
    if(false === isset($this->{$field})) {
      throw new \LogicException('Field is not defined');
    }

    if('set' == $match[1]) {
      $this->{$field} = $arguments[0];

      return $this;
    }

    return $this->{$field};
  }

}
