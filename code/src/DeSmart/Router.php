<?php
namespace DeSmart;

class Router {

  private $routes = array();

  /**
   * Dodaje nowa trase do konfiguracji
   *
   * W $pattern mozna zdefiniowac zmienne (zamykajac je w klamrach), ktore beda przekazane do callbacka
   *
   * @example
   *  $route->get('/foo/{id}', function($id) {});
   *
   * @param string $pattern
   * @param callbable $callback
   */
  public function get($pattern, $callback) {
    $this->routes[] = array(
      'regex' => $this->formatPattern($pattern),
      'callback' => $callback,
    );
  }

  public function redirect($url) {
    header('Location: '.$url);
  }

  public function run() {
    $path = $_SERVER['REQUEST_URI'];
    $match = array();

    foreach($this->routes as $route) {

      if(true == preg_match($route['regex'], $path, $match)) {
        array_shift($match);
        call_user_func_array($route['callback'], $match);

        break;
      }
    }
  }

  private function formatPattern($pattern) {
    return '#^'.preg_replace('/{\w+}/', '(\w+)', $pattern).'$#';
  }

}
