<?php

namespace App\Model;

use DeSmart\Model\AbstractRepo as BaseRepo;

class NewsRepo extends BaseRepo {

  private $news = array(
    array(
      'id' => '1',
      'title' => 'Drogi Marszałku',
      'text' => 'PKB rośnie. Nie muszę państwa przekonywać, że zmiana istniejących kryteriów wymaga niezwykłej precyzji w wypracowaniu nowych propozycji.',
      'date' => null,
      'tags' => array('foo', 'bar'),
    ),
    array(
      'id' => '2',
      'title' => 'Będzie lepiej.',
      'text' => 'Prawdą jest, iż zawiązanie koalicji koliduje z szerokim aktywem wymaga niezwykłej precyzji w określaniu obecnej sytuacji.',
      'date' => '2013-02-10 14:30:00',
      'tags' => array('bar'),
    ),
    array(
      'id' => '3',
      'title' => 'Różnorakie unowocześniania.',
      'text' => 'Gdy za sobą proces wdrożenia i bogate doświadczenia pozwalają na wiosnę...',
      'date' => '2013-02-14 09:00:00',
      'tags' => array('baz', 'bar', 'foo'),
    ),
    array(
      'id' => '4',
      'title' => 'Nikt inny.',
      'text' => 'Do tej sprawy pociąga za sobą proces wdrożenia i określenia systemu powszechnego uczestnictwa.',
      'date' => '2013-02-01 10:00:00',
      'tags' => array(),
    ),
  );

  public function getAll() {
    return array_map(array($this, 'hydrate'), $this->news);
  }

  public function getByTag($tag) {
    $data = array_filter($this->news, function($news) use ($tag) {
      return true === in_array($tag, $news['tags']);
    });

    return array_map(array($this, 'hydrate'), $data);
  }

  public function getById($id) {
    $data = array_filter($this->news, function($news) use ($id) {
      return true === ($id == $news['id']);
    });

    return array_map(array($this, 'hydrate'), $data);
  }
}
