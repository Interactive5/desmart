<?php

namespace App\Model;

use DeSmart\Model\AbstractEntity;

class News extends AbstractEntity
{

    protected $id;

    protected $title;

    protected $text;

    protected $date;

    protected $tags;

    /**
     * Metoda zwracaj�ca dat� w formie Y-m-d H:i:s dla newsa.
     * @return mixed
     */
    public function getDateTime()
    {
        return $this->date;
    }

    /**
     * Metoda zwracaj�ca dat� w formie Y-m-d dla newsa.
     * @return bool|string
     */
    public function getDate()
    {
        return ($this->date) ? date("Y-m-d", strtotime($this->date)) : '';
    }
} 
